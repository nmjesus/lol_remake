(function() {
    var PROTOCOL = window.location.protocol,
        MODULE = 'Ink.Ext.Lol.applyBindings',
        VERSION = '1',
        DEPS = [
            'Ink.Ext.Lol.Parser_1',
            'Ink.Ext.Lol.Event_1'
        ];

    Ink.setPath('Lol', PROTOCOL + '//localhost:3000/src/');

    Ink.createModule(MODULE, '1', DEPS, function(Parser, Event) {
        var applyBindings = function(inst, context) {
            if(typeof inst === 'undefined' || !inst) {
                throw new Error('Instance not defined.');
            }
            var data = Parser.objectify(Parser.parse(context));

            Event.subscribe('change:model', function(opt) {
                if(typeof opt.value === 'undefined') {
                    return;
                }
                var elm = opt.elm,
                prop = opt.prop,
                value = opt.value;

                if(typeof opt.elm[opt.prop] === 'undefined') {
                    elm.setAttribute(prop, value);
                } else {
                    opt.elm[opt.prop] = opt.value;
                }
            });

            for(var i = 0, len = data.length; i<len; i++) {
                for(var j = 0, l2 = data[i].dataset.length; j<l2; j++) {
                    for(var k in data[i].dataset[j]) {
                        var method = inst[data[i].dataset[j][k]],
                            defaultVal = method.apply(inst),
                            elm = data[i].elm;

                        inst[data[i].dataset[j][k]] = Ink.bind(function(elm, prop, method, val) {
                            var val = method.apply(inst, [val]);
                            Event.publish('change:model', {
                                elm: elm,
                                prop: k,
                                value: val
                            });
                            return val;
                        }, inst, elm, k, method);

                        Event.publish('change:model', {
                            elm: elm,
                            prop: k,
                            value: defaultVal
                        });
                    }
                }
            }


            return inst;
        };
        return applyBindings;
    });
}());
