Ink.createModule('Ink.Ext.Lol.Event', '1', [], function() {

    var events = {};

    var Event = {
        subscribe: function(eventName, fn) {
            if(!eventName) {
                return;
            }

            var fns = fn instanceof Array ? fn : [fn],
                len = fns.length;

            for(var i = 0; i<len; i++) {
                if(!fns[i] || typeof fns[i] !== 'function') {
                    return;
                }
            }

            if(!events[eventName]) {
                events[eventName] = [];
            }

            // append
            events[eventName].push.apply(events[eventName], fns);

            return events;
        },

        publish: function(eventName, message) {
            var ev = events[eventName],
                cb, i, len = ev ? ev.length : 0;

            for(i = 0; i<len; i++) {
                var cb = ev[i];
                try {
                    cb(message);
                } catch(e) {
                    console.error("Error calling " + eventName + ": " + e.message);
                }
            }
        },


        purge: function() {
            return (events = {});
        }
    };

    return Event;
});
