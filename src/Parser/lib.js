Ink.createModule('Ink.Ext.Lol.Parser', '1', [], function() {

    var isValidNode = function(elm) {
        return (elm && elm.nodeType === 1);
    };

    var Parser = {
        parse: function(dom) {
            var elms = Ink.ss('*[data-lol-bind]', dom || document.body);
            return elms;
        },
        
        objectify: function(elms) {
            if(!elms) {
                return [];
            }

            var data = [], dataSet, elm;

            for(var i = 0, len = elms.length; i<len; i++) {
                elm = elms[i];

                if(!isValidNode(elm) || !(dataSet = elm.getAttribute('data-lol-bind'))) {
                    continue;
                }

                data.push({ elm: elm, dataset: this.data(dataSet) });
            }
            return data;
        },


        data: function(dataSet) {
            if(!dataSet) {
                return [];
            }
            var idx, key, value;
            var data = dataSet.replace(/[\s\{\}]/g, '').split(/[;,]/g);
            for(var i = 0, len = data.length; i<len; i++) {
                idx = data[i].indexOf(':');
                key = data[i].substring(0, idx);
                value = data[i].substring(++idx, data[i].length);
                data[i] = JSON.parse('{"'+key+'": "'+value+'"}');
            }
            return data;
        }
    };

    return Parser;
});
