// Karma configuration
// Generated on Mon Sep 23 2013 18:23:05 GMT+0100 (WEST)

module.exports = function(config) {
  config.set({

    //plugins: [
    //  "karma-mocha",
    //  "karma-chai",
    //  "karma-phantom"
    //],

    // base path, that will be used to resolve files and exclude
    basePath: './',


    // frameworks to use
    frameworks: ['mocha', 'chai'],


    // list of files / patterns to load in the browser
    files: [
        { pattern: 'http://cdn.ink.sapo.pt/2.2.0/js/ink.min.js',  included: true },
        { pattern: 'http://sinonjs.org/releases/sinon-1.5.0.js', included: true },
        { pattern: 'src/**/*.js', included: true },
      'tests/**/*.js'
    ],


    preprocessors: {
        'src/**/*.js': ['coverage']
    },


    // list of files to exclude
    exclude: [
      '**/*.swp'
    ],


    // test results reporter to use
    // possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
    reporters: ['progress', 'coverage'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,

    autoWatchBatchDelay: 2000,


    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: [/*'Firefox', 'Opera',*/ 'PhantomJS'],


    // If browser does not capture in given timeout [ms], kill it
    captureTimeout: 60000,


    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false
  });
};
