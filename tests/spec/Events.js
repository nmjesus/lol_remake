var expect = chai.expect;


describe('Core', function() {
    describe('subscribe()', function() {
        var cb;

        beforeEach(function() {
            Ink.Ext.Lol.Event.purge();
        });

        before(function() {
            cb = function() {};
        });
        it('Should subscribe one event', function() {
            expect(Ink.Ext.Lol.Event.subscribe('foo', cb).foo).to.have.length(1);
        });
        it('Should throw an exception on invalid arguments', function() {
            expect(Ink.Ext.Lol.Event.subscribe(undefined, cb)).to.be.equal(undefined);
            expect(Ink.Ext.Lol.Event.subscribe('foo', undefined)).to.be.equal(undefined);
            expect(Ink.Ext.Lol.Event.subscribe(undefined, undefined)).to.be.equal(undefined);
        });
        it('Should set four events', function() {
            expect(Ink.Ext.Lol.Event.subscribe('foo', [cb, cb, cb, cb]).foo).to.have.length(4);
        });
    });

    describe('purge()', function() {
        it('Should purge all events subscribed', function() {
            var cb = function() {};
            Ink.Ext.Lol.Event.subscribe('foo', [cb, cb, cb, cb]);
            expect(Ink.Ext.Lol.Event.purge().foo).to.be.equal(undefined);
        });
    });

    describe('publish()', function() {
        it('Should publish one event', function() {
            var spy = sinon.spy();
            Ink.Ext.Lol.Event.subscribe('foo', spy);
            Ink.Ext.Lol.Event.publish('foo');
            expect(spy.calledOnce).to.be.equal(true);
        });
        it('Should publish one event and sent a message', function() {
            var spy = sinon.spy();
            Ink.Ext.Lol.Event.subscribe('foo', spy);
            Ink.Ext.Lol.Event.publish('foo', 'bar');
            expect(spy.calledWith('bar')).to.be.equal(true);
        });
        it('Should publish one event without any message', function() {
            var spy = sinon.spy();
            Ink.Ext.Lol.Event.subscribe('foo', spy);
            Ink.Ext.Lol.Event.publish('foo');
            expect(spy.args[1]).to.be.equal(undefined)
        });
        it('Should send a console.error when some function throws an exception', function() {
            var spy = sinon.spy(console, 'error');
            Ink.Ext.Lol.Event.subscribe('foo', function() { throw new Error("erro"); });
            Ink.Ext.Lol.Event.publish('foo');
            expect(spy.calledOnce).to.be.equal(true)
        });
    });
});
