var expect = chai.expect;

describe('Core', function() {
    describe('bind()', function() {
        var model, inst;

        beforeEach(function() {
            model = function() {};
            model.prototype = {};
            inst = new model();
        });

        it('Should return instance passed as argument', function() {
            expect(Ink.Ext.Lol.Bind(inst)).to.be.equal(inst);
        });

        it('Should throw an exception when instance is missing', function() {
            try {
                expect(Ink.Ext.Lol.Bind()).to.throw(Error);
            } catch(e) {}
        });
    });
});
