var expect = chai.expect;

describe('Parser', function() {
    describe('parse()', function() {
        var html = "",
            frag, container;

        before(function() {
            html = [
                '<h1 data-lol-bind="{ className: red }">Foo</h1>',
                '<h1 data-lol-bind="{ textContent: bar }">Foo</h1>',
            ].join('');
        });


        beforeEach(function() {
            frag = document.createDocumentFragment(),
            container = document.createElement('div');
        });

        it('Should return an array', function() {
            container.innerHTML = html;
            frag.appendChild(container);
            expect(Ink.Ext.Lol.Parser.parse(frag)).to.be.a('array');
        });

        it('Should return an array with length 2', function() {
            container.innerHTML = html;
            frag.appendChild(container);
            expect(Ink.Ext.Lol.Parser.parse(frag)).to.have.length(2);
        });

        it('Should return an empty array', function() {
            container.innerHTML = '';
            frag.appendChild(container);
            expect(Ink.Ext.Lol.Parser.parse(frag)).to.have.length(0);
        });

        it('Should return an array with length 2 when no argument is provided', function() {
            container.innerHTML = html;
            document.body.appendChild(container);
            expect(Ink.Ext.Lol.Parser.parse()).to.have.length(2);
        });

        after(function() {
            container.parentNode.removeChild(container);
        });
    });


    describe('objectify()', function() {
        var html = "",
            frag, container;

        before(function() {
            html = [
                '<h1 data-lol-bind="{ className: red }">Foo</h1>',
                '<h1 data-lol-bind="{ textContent: bar }">Foo</h1>',
            ].join('');
        });

        beforeEach(function() {
            frag = document.createDocumentFragment(),
            container = document.createElement('div');
        });

        it('Should return an array', function() {
            expect(Ink.Ext.Lol.Parser.objectify()).to.be.a('array');
        });

        it('Should return an empty array when no elements given', function() {
            expect(Ink.Ext.Lol.Parser.objectify()).to.have.length(0);
        });

        it('Should return an non empty array', function() {
            container.innerHTML = html;
            frag.appendChild(container);
            var elms = Ink.Ext.Lol.Parser.parse(frag);
            expect(Ink.Ext.Lol.Parser.objectify(elms)).to.be.not.empty;
        });

        it('Should return an array with 2 elements', function() {
            container.innerHTML = html;
            frag.appendChild(container);
            var elms = Ink.Ext.Lol.Parser.parse(frag);
            expect(Ink.Ext.Lol.Parser.objectify(elms)).to.have.length(2);
        });

        it('Should return an empty array due invalid nodes', function() {
            expect(Ink.Ext.Lol.Parser.objectify(['foo', 'bar'])).to.be.empty;
        });
    });


    describe('data()', function() {
        it('Should return data set parsed as json', function() {
            var databind = '{ className: red }';
            expect(Ink.Ext.Lol.Parser.data(databind)[0]).to.have.property('className');
            expect(Ink.Ext.Lol.Parser.data(databind)[0].className).to.be.equal('red');
        });

        it('Should return data with two properties properties', function() {
            var databind = '{ className: red; rel: rel }';
            expect(Ink.Ext.Lol.Parser.data(databind)).to.have.length(2);
        });

        it('Should return data with className and rel parsed', function() {
            var databind = '{ className: red; rel: rel }';
            expect(Ink.Ext.Lol.Parser.data(databind)[0]).to.have.property('className');
            expect(Ink.Ext.Lol.Parser.data(databind)[1]).to.have.property('rel');
        });

        it('Should split by , or ;', function() {
            var databind = '{ className: red; rel: rel }';
            expect(Ink.Ext.Lol.Parser.data(databind)).to.have.length(2);
        });

        it('Should return empty array on invalid databind format', function() {
            var databind;
            expect(Ink.Ext.Lol.Parser.data(databind)).to.have.length(0);
        });
    });
});
