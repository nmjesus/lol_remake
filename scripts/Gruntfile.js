/*global module:false*/

var APP_NAME        = 'Lol',
    TMP_EXTERNAL    = '/tmp/externalDeps.js';


module.exports = function(grunt) {

    var externalDeps = [
        'http://cdn.ink.sapo.pt/2.0.0/js/ink.min.js'
    ];

    var srcDeps = [
        '../src/*.js',
        '../src/**/*.js'
    ];


    // Project configuration.
    grunt.initConfig({
        meta: {
            version: '0.0.1'
        },


        curl: {
            long: {
                src: externalDeps,
                dest: ''
            }
        },


        concat: {
            dist: {
                src: [TMP_EXTERNAL].concat(srcDeps),
                dest: '../build/' + APP_NAME  + 'Bundle.js'
            }
        },


        uglify: {
            options: {
                report: 'min',
                mangle: true,
                beautify: false,
                wrap: true
            },
            dist: {
                src: '<%= concat.dist.dest %>',
                dest: '../build/' + APP_NAME  + '.min.js'
            }
        },

        jshint: {
            options: {
                curly: true,
                eqeqeq: true,
                immed: true,
                latedef: true,
                newcap: true,
                noarg: true,
                sub: true,
                undef: true,
                unused: true,
                boss: true,
                eqnull: true,
                browser: true,
                globals: {
                    Ink: true,
                    console: true,
                    s$: true, // document.getElementbyId wrapper,
                    s: true, // querySelector
                    ss: true, // querySelectorAll,
                    Lol: true
                }
            },

            gruntfile: {
                src: 'Gruntfile.js'
            },

            lib_test: {
                src: ['../src/**/*.js', '!../src/vendor/*.js']
            },
        },


        mocha_phantomjs: {
            all: {
                src: ['http://localhost:3000/tests/index.html'],
                options: {
                    urls: ['http://localhost:3000/tests/index.html'],
                    bail: true,
                    mocha: {
                        ignoreLeaks: false,
                        reporter: 'Spect',
                        run: true
                    },
                    globals: {
                        _: true
                    }
                }
            }
        },
    });

    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-curl');
    grunt.loadNpmTasks('grunt-mocha-phantomjs');

    // Default task.
    grunt.registerTask('default', ['curl', 'concat', 'jshint', 'uglify']);
    grunt.registerTask('test', ['mocha_phantomjs']);
};
